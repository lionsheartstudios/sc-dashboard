/**
 * This service class represents the SCServer for us
 */
import {SCServer} from '../config'
import { observable } from "mobx";

class SCService {

    _ws = null;
    @observable _status = "INIT";
    @observable _syncCode = null;
    @observable _noOfClients = 0;
    @observable _alertMessage = null;
    @observable _received = [];
    @observable _sent = [];

    get syncCode() { return this._syncCode; }
    set syncCode(syncCode) { this._syncCode = syncCode; }

    get noOfClients() { return this._noOfClients; }
    set noOfClients(noOfClients) { this._noOfClients = noOfClients; }

    get alertMessage() { return this._alertMessage; }
    set alertMessage(alertMessage) { this._alertMessage = alertMessage; }

    get received() { return this._received; }
    addToInbox(message) { this._received.unshift(message); }

    get sent() { return this._sent; }
    addToSent(message) { this._sent.unshift(message); }

    connect(syncCode){

        console.log(`${SCServer}${syncCode?'?gridId='+syncCode:''}`);
        this.ws = new WebSocket(`${SCServer}${syncCode?'?gridId='+syncCode:''}`);

        this.ws.onopen = () => {
            console.log("Websocket connected");
            this._status = "SOCKET_CONNECTED";
        };

        this.ws.onclose = () => {
            console.log("Websocket disconnected");
            this._status = "DISCONNECTED";
        };

        this.ws.onmessage = (evt) => {

            console.log("Message received : "+evt.data);
            var msg = JSON.parse(evt.data);
            this.addToInbox(msg);

            // this client was trying to connect
            if(this._status==="SOCKET_CONNECTED" && msg.status==="CLIENT_CONNECTED"){
                this._status="CLIENT_CONNECTED";
                this._syncCode = msg.gridId;
                this._noOfClients = msg.noOfClients;
                this._alertMessage = {type:'success', message:`Connected. ${this._noOfClients} clients in this group`};
                return;
            }

            // another client got connected
            if(this._status==="CLIENT_CONNECTED" && msg.status==="CLIENT_CONNECTED"){
                this._noOfClients = msg.noOfClients;
                this._alertMessage = {type:'success', message:`Another client joined. ${this._noOfClients} clients in this group`};
                return;
            }

            // another client left
            if(msg.status==="CLIENT_DISCONNECTED"){
                this._noOfClients = msg.noOfClients;
                this._alertMessage = {type:'warning', message:`Another client left. ${this._noOfClients} clients in this group`};
                return;
            }

            // another client left
            if(msg.status==="ERROR"){
                this._alertMessage = {type:'error', message:msg.message};
                return;
            }

        };

    }

    send(message){
        message.CommandId = this.createUUID();
        this.ws.send(JSON.stringify(message));
        this.addToSent(message);
    }

    disconnect(){
        this.ws.onclose = () => {
            console.log("Websocket disconnected");
            this._status=null;
            this._syncCode = null;
            this._noOfClients = null;
            this._alertMessage = {type:'error', message:`Disconnected from Server`};
        };
        this.ws.close();
    }

    createUUID() {
        var s = [];
        var hexDigits = "0123456789abcdef";
        for (var i = 0; i < 36; i++) {
            s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
        }
        s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
        s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
        s[8] = s[13] = s[18] = s[23] = "-";
        return s.join("");
    }

}

export default new SCService();