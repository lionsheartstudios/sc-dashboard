import React, { Component } from 'react';
import {MuiThemeProvider} from '@material-ui/core/styles'

import './App.css';

import theme from './theme';
import TopNav from './components/TopNav';
import Commander from './components/Commander';
import Alert from './components/Alert';
import Console from './components/Console';
import SCService from "./service/SCService";
import { observer } from "mobx-react"

class App extends Component {

    @observer
    render() {
        return (
            <MuiThemeProvider theme={theme}>
                <TopNav SCService={SCService}/>
                {SCService.syncCode && <div>
                    <Commander SCService={SCService}/>
                    <Console SCService={SCService}/>
                    <Alert SCService={SCService}/>
                </div>}
            </MuiThemeProvider>
        );
    }

}

export default App;
