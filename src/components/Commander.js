import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import JSONInput from 'react-json-editor-ajrm';
import locale from 'react-json-editor-ajrm/locale/en';
import Button from '@material-ui/core/Button';
import RunIcon from '@material-ui/icons/DirectionsRun';

import commands from '../commands'

const styles = {
    root: {
        position:'absolute',
        top:'4em',
        right: '0',
        width: '100%'
    },
    buttonHolder: {
        position: 'relative',
        padding: '1em 1.5em',
        width: '100%',
        overflow:'auto',
        whiteSpace: 'nowrap',
        borderTop: '2px solid grey',
        borderBottom: '2px solid grey'
    },
    run:{
        position:'absolute',
        bottom:'1.5em',
        right:'0.5em'
    },
    button: {
        marginRight: '2em'
    }
};

class Commander extends React.Component {

    state = { currCommand: null, sendCommand: null };

    render() {
        const { classes, SCService } = this.props;
        return (
            <div className={classes.root}>
                <div className={classes.buttonHolder}>
                    {commands.map( (command, i) => <Button variant="outlined" color="secondary" className={classes.button} key={i}
                        onClick={()=>this.setState(...this.state, {currCommand: command, sendCommand: command})}>{command.CommandType}</Button> )}
                </div>
                <JSONInput id='commander' placeholder={this.state.currCommand} locale={locale} width='100%' height='20em'
                           onChange={({jsObject})=>this.setState(...this.state,{sendCommand:jsObject})}/>
                <Button variant="fab" color="secondary" aria-label="Run" className={classes.run} onClick={()=>SCService.send(this.state.sendCommand)}
                        disabled={this.state.sendCommand===null||this.state.sendCommand===undefined}>
                    <RunIcon/>
                </Button>
            </div>
        );
    }
}

Commander.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Commander);
