import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Badge from '@material-ui/core/Badge';
import { observer } from "mobx-react"

import ConnectMenu from './ConnectMenu'

const styles = {
    root: {
        flexGrow: 1
    },
    topbar: {
        background: 'rgb(30,30,30)',
        boxShadow: 'none'
    },
    flex: {
        flexGrow: 1,
        marginTop:'0.15em',
        marginLeft:'0.35em',
        letterSpacing: '0.35em',
        fontSize: '1.5em',
        fontFamily: 'digital-7',
        color: 'rgb(93, 255, 0)'
    },
    iconRight: {
        right:'-0.5em'
    },
    noOfClients: {
        paddingRight: '1.5em'
    }
};

@observer
class TopNav extends React.Component {

    state = { showConnectMenu: false, anchorEl: null };

    render() {
        const { classes, SCService } = this.props;

        return (
            <div className={classes.root}>
                <AppBar position="fixed" className={classes.topbar}>
                    <Toolbar>
                        <Typography variant="title" color="inherit" className={classes.flex}>
                            {SCService.syncCode && SCService.syncCode}
                        </Typography>
                        {!SCService.syncCode && <Button color="inherit" onClick={(e)=>this.setState(...this.state, {anchorEl: e.currentTarget, showConnectMenu: true})}>
                            Connect</Button>}
                        {SCService.syncCode && <Button color="inherit" onClick={()=>SCService.disconnect()}>
                            <Badge color="secondary" badgeContent={SCService.noOfClients+""} className={classes.noOfClients}>Disconnect</Badge></Button>}
                    </Toolbar>
                </AppBar>
                <ConnectMenu open={!SCService.syncCode && this.state.showConnectMenu} anchorEl={this.state.anchorEl}
                           onClose={()=>this.setState(...this.state, {anchorEl: null, showConnectMenu: false})}/>
            </div>
        );
    }
}

TopNav.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TopNav);
