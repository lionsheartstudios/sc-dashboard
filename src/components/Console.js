import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ReactJson from 'react-json-view'
import { observer } from "mobx-react"

const styles = {
    root: {
        position: 'absolute',
        width: '100%',
        height: 'calc( 100% - 28em )',
        left: 0,
        bottom: 0,
        background: 'black',
        color: 'white'
    },
    box: {
        position: 'relative',
        height: '100%',
        maxHeight: '100%',
        width: '50%',
        display: 'inline-block',
        overflow: 'scroll',
        boxSizing: 'border-box',
        borderTop: '2px solid grey'
    },
    sent: {
        borderRight: '2px solid grey'
    },
    message: {
        margin: '1em 1em 0 1em'
    },
    boxTitle: {
        position: 'absolute',
        bottom: '6px',
        right: '6px',
        borderRadius: '4px',
        padding: '4px',
        color: 'rgb(93, 255, 0)',
        fontSize: '1em',
        fontFamily: 'digital-7'
    }
};

@observer
class Console extends React.Component {

    state = { showConsole: true };

    render() {
        const { classes, SCService } = this.props;

        return (
            <div className={classes.root}>
                <div className={`${classes.box} ${classes.sent}`}>
                    {SCService.sent.map((msg, i)=>(
                        <div key={i} className={classes.message}>
                            <ReactJson theme={'chalk'} src={msg} collapsed={true}/>
                        </div>
                    ))}
                    <div className={`${classes.boxTitle}`}>{`Tx ${SCService.sent.length}`}</div>
                </div>
                <div className={`${classes.box} ${classes.received}`}>
                    {SCService.received.map((msg, i)=>(
                        <div className={classes.message} key={i}>
                            <ReactJson theme={'chalk'} src={msg} collapsed={true}/>
                        </div>
                    ))}
                    <div className={`${classes.boxTitle}`}>{`Rx ${SCService.received.length}`}</div>
                </div>
            </div>
        );
    }
}

Console.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Console);
