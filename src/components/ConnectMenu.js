import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import SCService from '../service/SCService'

const styles = theme => ({
    connectMenu: {
        marginTop: '2.2em'
    },
    formControl: {
        margin: '1em 1.5em',
    },
    title: {
        margin: '1em 0 0 1em'
    },
    button: {
        margin: '1.5em auto 0.65em auto'
    },
    textField: {
        font: 'bold 13px/150% Arial, Helvetica, sans-serif',
        fontSize: '2em',
        width: '4em',
        maxLength: 5,
        letterSpacing: '0.2em'
    }
});

class ConnectMenu extends React.Component {

    state = { connectSettings: 'create', syncCode: null };

    render() {
        const { classes } = this.props;

        return (
            <Popover open={this.props.open} onClose={this.props.onClose} className={classes.connectMenu} anchorEl={this.props.anchorEl}
                     anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }} transformOrigin={{ vertical: 'bottom', horizontal: 'right'}} >
                <Typography variant="subheading" className={classes.title} color="secondary">Connection Options</Typography>
                <FormControl component="fieldset" className={classes.formControl}>
                    <RadioGroup name="connectSettingss" className={classes.group} value={this.state.connectSettings}
                                onChange={event => this.setState(...this.state, { connectSettings: event.target.value })}>
                        <FormControlLabel value="create" control={<Radio/>} checked={this.state.connectSettings==='create'} label="Create Grid" />
                        <FormControlLabel value="join" control={<Radio/>} checked={this.state.connectSettings==='join'} label="Join Grid" />
                        {this.state.connectSettings==='join' && <TextField className={classes.textField} InputProps={{classes:{input: classes.textField}}} helperText="grid-id"
                                   onInput = {(e) => {
                                       e.target.value = isNaN(e.target.value) ? "" : Math.max(0, parseInt(e.target.value, 10) ).toString().slice(0,5);
                                       this.setState(...this.state, {syncCode:e.target.value});
                                   }}/>}
                    </RadioGroup>
                    <Button variant="contained" color="secondary" className={classes.button}
                            disabled={this.state.connectSettings==='join' && (this.state.syncCode===null || this.state.syncCode.length<5)}
                            onClick={()=>{SCService.connect(this.state.syncCode); this.props.onClose()}}>Connect</Button>
                 </FormControl>
            </Popover>
        );
    }
}

ConnectMenu.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ConnectMenu);
