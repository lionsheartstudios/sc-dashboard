##Introduction

SC Dashboard is a React / MaterialUI App that does the following:

1. Connect to a running SCServer. `config.js` in the root dir tells the url / port for SCServer

2. Send `commands` to the server through websockets. There are all JSON commands.

3. Receive `commands` from the server through websockets.

4. Simulate multiplayer by using `gridId`

The primary purpose of this App is to act as a Testing Console to the ShadowClientServer.

##Installation

1. Install `Node.js v8.x` or above on your machine. 

2. From the root folder, run `npm install`. This will install all the packages.

3. From the root folder, run `npm run start`. This will start the dev server locally. You can access the app from `http://localhost:3000` 

##Build

From the root folder, run `npm run build`. This will create stuff in the folder `build`